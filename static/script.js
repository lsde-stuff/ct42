const button1 = document.getElementById('button1');
const text = document.getElementById('text');
const mixBlendModes = ['overlay', 'difference'];
var isMouseDown = false;
let i = 0;

button1.addEventListener('click', function(event) {
    text.style.filter = 'invert(100%)';
    text.style.mixBlendMode = mixBlendModes[i];
    i = (i + 1) % mixBlendModes.length;
});

button1.addEventListener('dblclick', function(event) {
    text.style.filter = 'none';
    text.style.mixBlendMode = 'normal';
});

button1.addEventListener('mousedown', function(event) {
    isMouseDown = true;
});

document.addEventListener('mouseup', function(event) {
    isMouseDown = false;
});

document.addEventListener('mousemove', function(event) {
    if(isMouseDown) {
        text.style.filter = `invert(${event.clientX % 100}%)`;
    }
});

