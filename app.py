#!/usr/bin/env python
import os
from flask import Flask, make_response
import requests

app = Flask(__name__)
app.debug = bool(int(os.getenv('APP_DEBUG', 0)))
listen_host = os.getenv('LISTEN_HOST', 'localhost')
listen_port = int(os.getenv('LISTEN_PORT', 5000))
api_baseurl = 'https://api.ceskatelevize.cz/video/v1/playlist-live/v1/stream-data/channel'

@app.route('/')
def root():
    return make_response(channel(24))


# a new route for switching channels
@app.route('/channel/<int:channel>')
def channel(channel):
    url = requests.get(f'{api_baseurl}/CH_{channel}?canPlayDrm=false&streamType=hls&quality=404p').json()
    return f'''<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ČT42</title>
    <link rel="stylesheet" href="/static/style.css">
    <link rel="stylesheet" href="/static/video-js.css"/>
    <link rel="icon"  type="image/jpg" href="/static/Ct42_img.jpg">
</head>
<body>
  <script src="/static/video.min.js"></script>
    <div id="container">
        <div class="tv-button" id="button1" style="top:267px"></div>
        <div class="tv-button" id="button2" style="top:283px"></div>
        <div class="tv-button" id="button3" style="top:300px"></div>
        <img id="text" src="/static/dont_panic.png">
        <img id="mask" src="/static/tv.png">
        <video class="video-js media-ivysilani-player" autoplay preload="auto" data-setup="">
            <source src="{url['streamUrls']['main']}" type="application/x-mpegURL" />
        </video>
    </div>
    <div id="linx">
       <a href="https://gitlab.com/jonassvatos-stuff/ct42">info</a>
    </div>
  <script src="/static/script.js"></script>
</body>
</html>'''


if __name__ == '__main__':
    app.run(host=listen_host, port=listen_port, threaded=True)
